import axios from "axios";

//set base URL || Agar tidak mengulangi penulisan url ditiap page
export const API = axios.create({
  baseURL: "http://localhost:5000/api/v1",
});

//mengintegrasikan default header untuk authentikasi
export const setAuthToken = (token) => {
  if (token) {
    API.defaults.headers.common["Authorization"] = `Bearer ${token}`;
  } else {
    delete API.defaults.headers.common["Authorization"];
  }
};

export const urlAsset = {
  img: "http://localhost:5000/src/uploads/img/",
};
