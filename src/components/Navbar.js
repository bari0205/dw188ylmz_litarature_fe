import React, { useState, useContext } from "react";
import "../App.css";
import "../literature.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Modal, Form } from "react-bootstrap";
import {
  BrowserRouter as Router,
  Switch,
  Link,
  Route,
  Redirect,
  useHistory,
  useRouteMatch,
} from "react-router-dom";
import { CartContext } from "../context/cartContext";

import { API, setAuthToken } from "../config/Api";
import { useQuery, useMutation } from "react-query";

import { FaRegUser, FaUserAlt } from "react-icons/fa";
import { FaBook } from "react-icons/fa";
import { FaBookMedical } from "react-icons/fa";
import { FaSignOutAlt } from "react-icons/fa";

import { BiBookAdd, BiExit } from "react-icons/bi";
import { TiThLis } from "react-icons/ti";
import { VscLibrary } from "react-icons/vsc";

export default function Navbar() {
  const [state, dispatch] = useContext(CartContext);
  const history = useHistory();

  return (
    <div>
      <div
        className="card-header"
        style={{ backgroundColor: "black", height: "100px" }}
      >
        <div className="container" style={{ marginLeft: "2%" }}>
          <div className="row">
            <div className="col col-md-6" style={{ marginTop: "3%" }}>
              <Link
                to="/profile"
                className="linkmenu"
                style={{ textDecoration: "none" }}
              >
                <p className="font-menu">Profile</p>
              </Link>
              <Link
                to="/library"
                className="linkmenu1"
                style={{ textDecoration: "none" }}
              >
                <p className="font-menu">My Collection</p>
              </Link>
              <Link
                to="/add"
                className="linkmenu1"
                style={{ textDecoration: "none" }}
              >
                <p className="font-menu">Add Literatur</p>
              </Link>
              <Link
                onClick={() =>
                  dispatch({
                    type: "LOGOUT",
                  })
                }
                className="linkmenu1"
                style={{ textDecoration: "none" }}
              >
                <p className="font-menu">Logout</p>
              </Link>
            </div>

            <div className="col col-md-6">
              <div style={{ textAlign: "right", marginRight: "10%" }}>
                <Link to="/first">
                  <img src={require("../images/iconliterature.png")}></img>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
