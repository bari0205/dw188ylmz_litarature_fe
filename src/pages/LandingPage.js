import React, { Component, useState, useContext } from "react";
import ReactDOM from "react-dom";
import "../App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Modal, Form } from "react-bootstrap";
import {
  BrowserRouter as Router,
  Switch,
  Link,
  Route,
  Redirect,
  useHistory,
} from "react-router-dom";
import { CartContext } from "../context/cartContext";
import { API, setAuthToken } from "../config/Api";
import { useQuery, useMutation } from "react-query";
// Import Page
import Mainpage from "./Mainpage";

const inputform = {
  bginput: {
    backgroundColor: "rgba(210, 210, 210, 0.25)",
    height: "50px",
  },
};

const buttonpunya = {
  bgbutton: {
    width: "350px",
    height: "50px",
    left: "33px",
    top: "555px",
    background: "#EE4622",
    borderRadius: "5px",
    borderColor: "#EE4622",
  },
};

const noacc = {
  bentuk: {
    width: "276px",
    height: "25px",
    top: "346px",

    fontFamily: "Avenir",
    fontStyle: "normal",
    fontWeight: "normal",
    fontSize: "18px",
    lineHeight: "60px",
    /* identical to box height */

    color: "#333333",
  },
};

export default function App() {
  const history = useHistory();
  const [state, dispatch] = useContext(CartContext);
  const [modalSignup, setModalSignup] = useState(false);
  const [modalSignin, setModalSignin] = useState(false);

  const [formData, setFormData] = useState({
    email: "",
    password: "",
    fullname: "",
    gender: "",
    phone: "",
    address: "",
  });

  const handleSignin = () => {
    setModalSignin(true);
    setModalSignup(false);
  };

  const handleSignup = () => {
    setModalSignin(false);
    setModalSignup(true);
  };

  const { email, password, fullName, gender, phone, address } = formData;

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSubmitSignin = async (e) => {
    e.preventDefault();
    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    };

    const body = JSON.stringify({ email, password });

    try {
      const res = await API.post("/login", body, config);

      dispatch({
        type: "LOGIN_SUCCESS",
        payload: res.data.data,
      });

      console.log(res);

      setAuthToken(res.data.data.token);

      try {
        const res = await API.get("/checkAuth");

        dispatch({
          type: "USER_LOADED",
          payload: res.data.data.user,
        });
      } catch (err) {
        dispatch({
          type: "AUTH_ERROR",
        });
      }

      if (email == "admin@root.com") {
        history.push(`/admin`);
      } else {
        history.push(`/first`);
      }
    } catch (err) {
      dispatch({
        type: "LOGIN_FAIL",
      });
    }
  };

  const [register] = useMutation(async () => {
    try {
      const config = {
        headers: {
          "Content-Type": "application/json",
        },
      };
      const body = JSON.stringify({
        fullName,
        email,
        gender,
        password,
        phone,
        address,
      });

      const res = await API.post("/register", body, config);

      dispatch({
        type: "LOGIN_SUCCESS",
        payload: res.data.data,
      });

      console.log(res);

      setAuthToken(res.data.data.token);

      try {
        const res = await API.get("/checkAuth");

        dispatch({
          type: "USER_LOADED",
          payload: res.data.data.user,
        });
      } catch (err) {
        dispatch({
          type: "AUTH_ERROR",
        });
      }
      if (email == "admin@root.com") {
        history.push(`/admin`);
      } else {
        history.push(`/first`);
      }
    } catch (err) {
      console.log(err);
    }
  });

  return (
    <div className="Container" style={{ backgroundColor: "black" }}>
      <div className="row">
        <div className="col col-md-6" style={{ paddingRight: "0px" }}>
          <img
            src={require("../images/iconliterature.png")}
            style={{ margin: "6% 12%", width: "26%" }}
          ></img>
          <div
            style={{
              width: "120%",
              marginLeft: "12%",
              fontFamily: "Times New Roman",
              fontWeight: "bold",
              fontSize: "110px",
              lineHeight: "82.49%",
              color: "white",
            }}
          >
            <i>source of </i> intelligence
          </div>
          <div
            style={{
              width: "75%",
              marginLeft: "12%",
              marginTop: "5%",
              lineHeight: "33px",
              fontSize: "24px",
              color: "white",
            }}
          >
            Sign-up and receive unlimited accesss to all of your literatur -
            share your literature.
          </div>
          <div style={{ marginLeft: "12%", marginTop: "3%" }}>
            <div className="row">
              <div className="col col-md-5">
                <Button
                  type="button"
                  onClick={() => setModalSignup(true)}
                  id="signup"
                  className="btn"
                  style={{
                    borderColor: "#AF2E1C",
                    width: "100%",
                    height: "50px",
                    backgroundColor: "#AF2E1C",
                    color: "white",
                    fontFamily: "Avenir",
                    fontSize: "18px",
                  }}
                >
                  Sign Up
                </Button>
              </div>
              <div className="col col-md-5">
                <Button
                  onClick={() => handleSignin()}
                  className="btn"
                  style={{
                    width: "100%",
                    height: "50px",
                    backgroundColor: "#F9F9F9",
                    borderColor: "#F9F9F9",
                    color: "black",
                    fontFamily: "Avenir",
                    fontSize: "18px",
                  }}
                >
                  Sign In
                </Button>
              </div>
            </div>
          </div>
        </div>

        <div className="col col-md-6" style={{ paddingLeft: "0px" }}>
          <img src={require("../images/coverLanding.png")}></img>
        </div>
      </div>

      {/* Modal signup */}
      <Modal show={modalSignup} onHide={() => setModalSignup(false)}>
        <Modal.Header closeButton style={{ borderBottom: "white" }}>
          {/* <Modal.Title id="example-modal-sizes-title-sm">
            Small Modal
          </Modal.Title> */}
          <div className="titlemodal">Sign Up</div>
        </Modal.Header>
        <Modal.Body style={{ textAlign: "center" }}>
          <Form
            onSubmit={(e) => {
              e.preventDefault();
              register();
            }}
          >
            <Form.Group controlId="">
              <Form.Control
                type="email"
                name="email"
                placeholder="Email"
                style={inputform.bginput}
                value={email}
                onChange={(e) => handleChange(e)}
              />
            </Form.Group>
            <Form.Group controlId="formGroupPassword">
              <Form.Control
                type="password"
                name="password"
                placeholder="Password"
                style={inputform.bginput}
                value={password}
                onChange={(e) => handleChange(e)}
              />
            </Form.Group>
            <Form.Group controlId="formGroupPassword">
              <Form.Control
                type="text"
                name="fullName"
                placeholder="Full Name"
                style={inputform.bginput}
                value={fullName}
                onChange={(e) => handleChange(e)}
              />
            </Form.Group>
            <Form.Group controlId="formGroupPassword">
              <Form.Control
                as="select"
                name="gender"
                style={inputform.bginput}
                value={gender}
                onChange={(e) => handleChange(e)}
                required
              >
                <option value="">Gender</option>
                <option value="Male">Male</option>
                <option value="Woman">Woman</option>
              </Form.Control>
            </Form.Group>
            <Form.Group controlId="formGroupPassword">
              <Form.Control
                type="number"
                name="phone"
                placeholder="Phone"
                style={inputform.bginput}
                value={phone}
                onChange={(e) => handleChange(e)}
              />
            </Form.Group>
            <Form.Group controlId="formGroupPassword">
              <Form.Control
                as="textarea"
                name="address"
                placeholder="Address"
                style={inputform.bginput}
                value={address}
                onChange={(e) => handleChange(e)}
              />
            </Form.Group>
            <Button
              type="submit"
              variant="primary"
              style={buttonpunya.bgbutton}
            >
              Sign Up
            </Button>
          </Form>
          <font style={noacc.bentuk}>
            Do Have Account ? Click{" "}
            <Link onClick={() => handleSignin()} style={{ color: "black" }}>
              {" "}
              <b>Here</b>{" "}
            </Link>
          </font>
        </Modal.Body>
      </Modal>

      {/* Modal signin */}
      <Modal show={modalSignin} onHide={() => setModalSignin(false)}>
        <Modal.Header closeButton style={{ borderBottom: "white" }}>
          {/* <Modal.Title id="example-modal-sizes-title-sm">
            Small Modal
          </Modal.Title> */}
          <div className="titlemodal">Sign In</div>
        </Modal.Header>
        <Modal.Body style={{ textAlign: "center" }}>
          <Form onSubmit={(e) => handleSubmitSignin(e)}>
            <Form.Group controlId="">
              <Form.Control
                type="email"
                name="email"
                placeholder="Email"
                style={inputform.bginput}
                value={email}
                onChange={(e) => handleChange(e)}
              />
            </Form.Group>
            <Form.Group controlId="formGroupPassword">
              <Form.Control
                type="password"
                name="password"
                placeholder="Password"
                style={inputform.bginput}
                value={password}
                onChange={(e) => handleChange(e)}
              />
            </Form.Group>
            <Button
              type="submit"
              variant="primary"
              style={buttonpunya.bgbutton}
            >
              Sign In
            </Button>
          </Form>
          <br></br>
          <br></br>
          <font style={noacc.bentuk}>
            Don't Have Account ? Click{" "}
            <Link onClick={() => handleSignup()} style={{ color: "black" }}>
              {" "}
              <b>Here</b>{" "}
            </Link>
          </font>
        </Modal.Body>
      </Modal>
    </div>
  );
}
