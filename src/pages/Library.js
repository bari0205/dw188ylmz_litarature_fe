import React, { useState, useContext } from "react";
import { CartContext } from "../context/cartContext";
import { Link, useHistory } from "react-router-dom";
import { useQuery } from "react-query";
import { API } from "../config/Api";
export default function Library() {
  const history = useHistory();
  const [state, dispatch] = useContext(CartContext);

  const { isLoading, error, data: bookMark, refetch } = useQuery(
    "getMark",
    () => API.get(`/mark/${state.user.id}`)
  );
  return isLoading || !bookMark ? (
    <h1>Loading..</h1>
  ) : (
    <div>
      <div className="container">
        <p className="listBook">My Literature</p>
        <div className="row">
          {bookMark.data.data.detail.map((mark) => (
            <div className="col col-md-3">
              <Link
                style={{ textDecoration: "none" }}
                onClick={() => history.push(`/detail/${mark.bookId}`)}
              >
                <div>
                  <img
                    className="gambarbuku"
                    src={require(`../images/${mark.libraryBook.image}`)}
                  />
                  <p className="judulbuku">{mark.libraryBook.title}</p>
                  <p className="pengarang">
                    <diV
                      className="col col-md-8"
                      style={{ padding: "0", display: "inline-block" }}
                    >
                      {mark.libraryBook.title}
                    </diV>

                    <diV
                      className="col col-md-4"
                      style={{
                        textAlign: "right",
                        padding: "0",
                        display: "inline-block",
                      }}
                    >
                      {mark.libraryBook.publication.split(`-`)[0]}
                    </diV>
                  </p>
                </div>
              </Link>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
