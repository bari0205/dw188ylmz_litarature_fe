import React, { useState } from "react";
import { Button, Modal, Form } from "react-bootstrap";
import { useHistory } from "react-router-dom";
import { BiSearchAlt2 } from "react-icons/bi";

export default function Firstpage() {
  const history = useHistory();

  const [formData, setFormData] = useState({
    search: "",
  });

  const { search } = formData;

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  function handleSeacrh() {
    history.push(`/main/${search}`);
  }

  return (
    <div>
      <div style={{ textAlign: "center", marginTop: "5.6%" }}>
        <img src={require("../images/Bigiconliterature.png")}></img>
      </div>
      <div style={{ width: "100%", marginTop: "7%", textAlign: "center" }}>
        <Form onSubmit={() => handleSeacrh()}>
          <Form.Group
            controlId=""
            style={{
              display: "inline-block",
              width: "40%",
            }}
          >
            <Form.Control
              type="text"
              placeholder="Search"
              name="search"
              value={search}
              onChange={(e) => handleChange(e)}
              required
            />
          </Form.Group>
          <Button type="submit" className="buttonsearch">
            <BiSearchAlt2 />
          </Button>
        </Form>
      </div>
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
      <br />
    </div>
  );
}
