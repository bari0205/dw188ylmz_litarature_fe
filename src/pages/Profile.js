import React, { useState, useContext } from "react";
import "../App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Modal, Form } from "react-bootstrap";
import { CartContext } from "../context/cartContext";

import { FaRegEnvelope } from "react-icons/fa";
import { FaBook } from "react-icons/fa";
import { FaBookMedical } from "react-icons/fa";
import { FaSignOutAlt } from "react-icons/fa";
import { FaTransgender } from "react-icons/fa";
import { FaRegAddressBook } from "react-icons/fa";
import { FaMapMarkerAlt } from "react-icons/fa";

import { useQuery, useMutation } from "react-query";
import { API, urlAsset } from "../config/Api";

import { Link, useHistory } from "react-router-dom";

export default function Profile() {
  const history = useHistory();

  const [state, dispatch] = useContext(CartContext);
  const [modalUpdate, setModalUpdate] = useState(false);
  const [image, setImage] = useState(null);

  const {
    isLoading,
    error,
    data: profileData,
    refetch,
  } = useQuery("getProfile", () => API.get(`/user/${state.user.id}`));

  const { data: bookUser } = useQuery("getBookUser", () =>
    API.get(`/book-user/${state.user.id}`)
  );

  const [updateImage] = useMutation(async () => {
    try {
      const config = {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      };
      const formData = new FormData();
      formData.append("image", image, image.name);

      setModalUpdate(false);
      const res = await API.patch(`/user/${state.user.id}`, formData, config);
      refetch();
      return res;
    } catch (err) {
      refetch();
      console.log(err);
    }
  });

  return isLoading || !profileData || !bookUser ? (
    <h1>Loading...</h1>
  ) : error ? (
    <h1>Your Error : {error.message}</h1>
  ) : (
    <div>
      <p className="listBook" style={{ marginLeft: "5%" }}>
        Profile
      </p>
      <div
        className="card-header"
        style={{
          height: "60%",
          width: "90%",
          backgroundColor: "#252525",
          margin: "auto",
          borderRadius: "10px",
        }}
      >
        <div className="container">
          <div className="row">
            <div className="col col-md-8">
              <div style={{ marginLeft: "8%", marginTop: "3%" }}>
                <div>
                  <div style={{ display: "inline" }}>
                    <FaRegEnvelope color="#AF2E1C" size={20} />
                  </div>
                  <p className="judulprofile" style={{ display: "inline" }}>
                    {profileData.data.data.detail.email}
                  </p>
                  <p className="subjudulprofile" style={{ display: "block" }}>
                    Email
                  </p>
                </div>

                <div>
                  <div style={{ display: "inline" }}>
                    <FaTransgender color="#AF2E1C" size={20} />
                  </div>
                  <p className="judulprofile" style={{ display: "inline" }}>
                    {profileData.data.data.detail.gender}
                  </p>
                  <p className="subjudulprofile" style={{ display: "block" }}>
                    Gender
                  </p>
                </div>

                <div>
                  <div style={{ display: "inline" }}>
                    <FaRegAddressBook color="#AF2E1C" size={20} />
                  </div>
                  <p className="judulprofile" style={{ display: "inline" }}>
                    {profileData.data.data.detail.phone}
                  </p>
                  <p className="subjudulprofile" style={{ display: "block" }}>
                    Contact
                  </p>
                </div>

                <div>
                  <div style={{ display: "inline" }}>
                    <FaMapMarkerAlt color="#AF2E1C" size={20} />
                  </div>
                  <p className="judulprofile" style={{ display: "inline" }}>
                    {profileData.data.data.detail.address}
                  </p>
                  <p className="subjudulprofile" style={{ display: "block" }}>
                    Address
                  </p>
                </div>
              </div>
            </div>
            <div className="col col-md-3">
              <div>
                <img
                  className="gambarprofile"
                  src={urlAsset.img + profileData.data.data.detail.image}
                />
              </div>
              <div>
                <button
                  className="btn gantipro"
                  style={{ backgroundColor: "#EE4622", color: "White" }}
                  onClick={() => setModalUpdate(true)}
                >
                  <font className="tulisanganti">Change Profile</font>
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container" style={{ paddingLeft: "0px" }}>
        <p className="listBook">My Literature</p>
        <div className="row">
          {bookUser.data.data.detail.map((book) => (
            <div className="col col-md-3" style={{ marginTop: "3%" }}>
              <Link
                style={{ textDecoration: "none" }}
                onClick={() => history.push(`/detail/${book.id}`)}
              >
                {book.status == "Aproved" ? (
                  <div>
                    <img
                      className="gambarbuku"
                      src={require(`../images/${book.image}`)}
                    />
                    <p className="judulbuku">{book.title}</p>
                    <p className="pengarang">
                      <diV
                        className="col col-md-8"
                        style={{ padding: "0", display: "inline-block" }}
                      >
                        {book.author}
                      </diV>

                      <diV
                        className="col col-md-4"
                        style={{
                          textAlign: "right",
                          padding: "0",
                          display: "inline-block",
                        }}
                      >
                        {book.publication.split(`-`)[0]}
                      </diV>
                    </p>
                  </div>
                ) : book.status == "Waiting" ? (
                  <div
                    style={{
                      backgroundColor: "#8080804a",
                      textAlign: "center",
                      borderRadius: "10px",
                      height: "100%",
                    }}
                  >
                    <img
                      className="gambarbuku"
                      style={{ opacity: "0.4" }}
                      src={require(`../images/${book.image}`)}
                    />
                    <span className="waiting">Waiting to be verified</span>
                    <p
                      style={{ opacity: "0.4", textAlign: "left" }}
                      className="judulbuku"
                    >
                      {book.title}
                    </p>
                    <p
                      style={{ opacity: "0.4", textAlign: "left" }}
                      className="pengarang"
                    >
                      <diV
                        className="col col-md-8"
                        style={{ padding: "0", display: "inline-block" }}
                      >
                        {book.author}
                      </diV>

                      <diV
                        className="col col-md-4"
                        style={{
                          textAlign: "right",
                          padding: "0",
                          display: "inline-block",
                        }}
                      >
                        {book.publication.split(`-`)[0]}
                      </diV>
                    </p>
                  </div>
                ) : (
                  <div
                    style={{
                      backgroundColor: "#8080804a",
                      textAlign: "center",
                      borderRadius: "10px",
                      height: "100%",
                    }}
                  >
                    <img
                      className="gambarbuku"
                      style={{ opacity: "0.4" }}
                      src={require(`../images/${book.image}`)}
                    />
                    <span className="canceled">Canceled</span>
                    <p
                      style={{ opacity: "0.4", textAlign: "left" }}
                      className="judulbuku"
                    >
                      {book.title}
                    </p>
                    <p
                      style={{ opacity: "0.4", textAlign: "left" }}
                      className="pengarang"
                    >
                      <diV
                        className="col col-md-8"
                        style={{ padding: "0", display: "inline-block" }}
                      >
                        {book.author}
                      </diV>

                      <diV
                        className="col col-md-4"
                        style={{
                          textAlign: "right",
                          padding: "0",
                          display: "inline-block",
                        }}
                      >
                        {book.publication.split(`-`)[0]}
                      </diV>
                    </p>
                  </div>
                )}
              </Link>
            </div>
          ))}
        </div>
      </div>
      <br />
      <br />
      <br />
      <Modal
        size="sm"
        show={modalUpdate}
        onHide={() => setModalUpdate(false)}
        dialogClassName="modal-90w posisimodal"
        contentClassName="besardialog"
        aria-labelledby="example-custom-modal-styling-title"
      >
        <Modal.Body>
          <Form
            onSubmit={(e) => {
              e.preventDefault();
              updateImage();
            }}
          >
            <Form.Group controlId="">
              <Form.Control
                type="file"
                placeholder="Profile Image"
                name="image"
                onChange={(e) => setImage(e.target.files[0])}
              />
            </Form.Group>
            <div style={{ float: "right" }}>
              <Button
                type="submit"
                variant=""
                style={{ backgroundColor: "#EE4622" }}
              >
                <font className="namabutton">Change Image</font>
              </Button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>
    </div>
  );
}
