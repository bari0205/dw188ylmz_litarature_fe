import React, { useContext, useState } from "react";
import "../App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { Button, Modal, Form } from "react-bootstrap";
import { FaBookMedical } from "react-icons/fa";
import { CartContext } from "../context/cartContext";

import { useQuery, useMutation } from "react-query";
import { API } from "../config/Api";

export default function AddBook() {
  const [modalAdd, setModalAdd] = useState(false);
  const [state, dispatch] = useContext(CartContext);

  const [formData, setFormData] = useState({
    userId: `${state.user.id}`,
    categoryId: "",
    title: "",
    author: "",
    publication: "",
    page: "",
    ISBN: "",
    aboutBook: "",
    file: "",
    status: "Waiting",
    image: "",
  });

  const {
    userId,
    categoryId,
    title,
    author,
    publication,
    page,
    ISBN,
    aboutBook,
    file,
    status,
    image,
  } = formData;

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const [addBook] = useMutation(async () => {
    try {
      const config = {
        headers: {
          "Content-Type": "application/json",
        },
      };
      const body = JSON.stringify({
        title,
        userId,
        author,
        publication,
        page,
        ISBN,
        aboutBook,
        file,
        status,
        image,
      });
      const res = await API.post("/books", body, config);

      setFormData({
        userId: `${state.user.id}`,
        title: "",
        author: "",
        publication: "",
        page: "",
        ISBN: "",
        aboutBook: "",
        file: "",
        status: "Waiting",
        image: "",
      });

      setModalAdd(true);

      return res;
    } catch (err) {
      console.log(err);
    }
  });

  return (
    <div>
      <div>
        <p className="addbook">Add Literature</p>
      </div>

      <div style={{ width: "90%", margin: "auto" }}>
        <Form
          onSubmit={(e) => {
            e.preventDefault();
            addBook();
          }}
        >
          <Form.Group controlId="">
            <Form.Control
              type="text"
              placeholder="Title"
              name="title"
              value={title}
              onChange={(e) => handleChange(e)}
            />
          </Form.Group>
          <Form.Group controlId="">
            <Form.Control
              type="date"
              placeholder="Publication"
              name="publication"
              value={publication}
              onChange={(e) => handleChange(e)}
            />
          </Form.Group>
          <Form.Group controlId="">
            <Form.Control
              type="text"
              placeholder="Author"
              name="author"
              value={author}
              onChange={(e) => handleChange(e)}
            />
          </Form.Group>

          <Form.Group controlId="">
            <Form.Control
              type="text"
              placeholder="Pages"
              name="page"
              value={page}
              onChange={(e) => handleChange(e)}
            />
          </Form.Group>
          <Form.Group controlId="">
            <Form.Control
              type="text"
              placeholder="ISBN"
              name="ISBN"
              value={ISBN}
              onChange={(e) => handleChange(e)}
            />
          </Form.Group>

          <Form.Group controlId="">
            <Form.Control
              type="text"
              placeholder="File Book"
              name="file"
              value={file}
              onChange={(e) => handleChange(e)}
            />
          </Form.Group>

          <Form.Group controlId="">
            <Form.Control
              type="text"
              placeholder="Image Book"
              name="image"
              value={image}
              onChange={(e) => handleChange(e)}
            />
          </Form.Group>

          <Form.Group controlId="">
            <Form.Control
              type="text"
              placeholder="Image Book"
              name="userId"
              value={userId}
              onChange={(e) => handleChange(e)}
              hidden
            />
          </Form.Group>

          <Form.Group controlId="">
            <Form.Control
              type="text"
              placeholder="Image Book"
              name="status"
              value={status}
              onChange={(e) => handleChange(e)}
              hidden
            />
          </Form.Group>

          <div style={{ float: "right" }}>
            <Button
              type="submit"
              variant=""
              style={{ backgroundColor: "#EE4622" }}
              onClick={() => setModalAdd(true)}
            >
              <font className="namabutton">
                Add Literature <FaBookMedical />
              </font>
            </Button>
          </div>
        </Form>
      </div>
      <br></br>
      <br></br>
      <br></br>
      <br></br>
      <br></br>

      <Modal
        size="lg"
        show={modalAdd}
        onHide={() => setModalAdd(false)}
        dialogClassName="modal-90w posisimodal"
        contentClassName="besardialog"
        aria-labelledby="example-custom-modal-styling-title"
      >
        <Modal.Body>
          <div className="ketmod">
            <p style={{ width: "100%" }}>
              Thank you for adding your own literature to our website, please
              wait 1 x 24 hours to verify whether this book is your writing
            </p>
          </div>
        </Modal.Body>
      </Modal>
    </div>
  );
}
